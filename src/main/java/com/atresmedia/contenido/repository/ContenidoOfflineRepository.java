package com.atresmedia.contenido.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atresmedia.contenido.bean.ContenidoBean;

public interface ContenidoOfflineRepository extends JpaRepository<ContenidoBean, Long> {

}

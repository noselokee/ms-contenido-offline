package com.atresmedia.contenido.service;

import java.util.List;

import com.atresmedia.contenido.bean.ContenidoBean;

public interface ContenidoOfflineService {
	
	List<ContenidoBean> save(ContenidoBean contenido);
	
	List<ContenidoBean> findAll();
	
	List<ContenidoBean> delete(Long id);
	
	List<ContenidoBean> update(ContenidoBean contenido);

}

package com.atresmedia.contenido.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atresmedia.contenido.bean.ContenidoBean;
import com.atresmedia.contenido.repository.ContenidoOfflineRepository;
import com.atresmedia.contenido.service.ContenidoOfflineService;

@Service
public class ContenidoOfflineServiceImpl {
	
	@Autowired
	private ContenidoOfflineRepository contenidoOfflineRepository;
		
	
	public List<ContenidoBean> save(ContenidoBean contenidoEntrada){
		
		contenidoEntrada.setCreationDate(new Date());
		contenidoEntrada.setModificationDate(new Date());
		contenidoOfflineRepository.save(contenidoEntrada);
		List<ContenidoBean> contenido = findAll();
		return contenido;
		
	}
	

	public List<ContenidoBean> findAll(){
		
		List<ContenidoBean> contenido = new ArrayList<>();
		contenido = contenidoOfflineRepository.findAll();
		return contenido;
		
	}
	
	
	public List<ContenidoBean> delete(Long id){
		
		ContenidoBean contenidoEliminar = new ContenidoBean();
		contenidoEliminar = contenidoOfflineRepository.getOne(id);
		contenidoOfflineRepository.delete(contenidoEliminar);
		List<ContenidoBean> contenido = findAll();
		return contenido;
		
	}
	
	
	public List<ContenidoBean> update(ContenidoBean contenidoEntrada){
		List<ContenidoBean> contenido = save(contenidoEntrada);
		return contenido;
		
	}
	
}

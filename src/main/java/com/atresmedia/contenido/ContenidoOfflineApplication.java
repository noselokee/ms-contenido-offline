package com.atresmedia.contenido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContenidoOfflineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContenidoOfflineApplication.class, args);
	}

}

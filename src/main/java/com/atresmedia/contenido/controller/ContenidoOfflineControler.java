package com.atresmedia.contenido.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atresmedia.contenido.bean.ContenidoBean;
import com.atresmedia.contenido.service.ContenidoOfflineService;
import com.atresmedia.contenido.service.impl.ContenidoOfflineServiceImpl;

@RestController
@RequestMapping({"/api-contenido"})
public class ContenidoOfflineControler {
	
	@Autowired
	private ContenidoOfflineServiceImpl contenidoOfflineService;
	
	@PutMapping(value = "/add")
	@ResponseBody
	public List<ContenidoBean> anyadirContenido(@Valid @RequestBody ContenidoBean contenidoEntrada) {
		
		List<ContenidoBean> contenido = contenidoOfflineService.save(contenidoEntrada);
		return contenido;
		
	}
	
	@DeleteMapping("/delete/{idContenido}")
	public List<ContenidoBean> eliminarContenido(
			@PathVariable("idContenido") Long idContenido) {
		
		List<ContenidoBean> contenido = contenidoOfflineService.delete(idContenido);
		return contenido;
		
	}
	
	@PatchMapping(value = "/update/{idContenido}")
	@ResponseBody
	public List<ContenidoBean> modificarContenido(@Valid @RequestBody ContenidoBean contenidoEntrada){
		
		List<ContenidoBean> contenido = contenidoOfflineService.update(contenidoEntrada);	
		return contenido;
		
	}
	
	@GetMapping(value = "/list")
	public List<ContenidoBean> listarContenidos(){	
		
		List<ContenidoBean> contenido = contenidoOfflineService.findAll();
		return contenido;
		
	}

}

package com.atresmedia.contenido.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="contents")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContenidoBean implements Serializable {
	
	private static final long serialVersionUID = -594981585273904719L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContent;
	
    @NotEmpty
	private String  name;
    
    private String description;
	
    @NotEmpty
	private Date creationDate;
    
    @NotEmpty
    private Date modificationDate;
    
    private String channel;
    
    private String type;
    
    private String format;
    
}
